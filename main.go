package main

import (
	"os"
	"strconv"
	"syscall"

	"github.com/k0kubun/pp"
	"gitlab.com/diamondburned/memanal/proc"
)

func main() {
	pid, err := strconv.Atoi(os.Args[1])
	if err != nil {
		panic(err)
	}

	p, err := proc.New(pid)
	if err != nil {
		panic(err)
	}

	if err := syscall.PtraceAttach(p.PID); err != nil {
		panic(err)
	}

	mf, err := p.GetMaps()
	if err != nil {
		panic(err)
	}

	m, err := os.Open("/proc/" + os.Args[1] + "/mem")
	if err != nil {
		panic(err)
	}

	var regions = make([][]byte, 0, len(mf))
	//var s strings.Builder
	//var r = bufio.NewReader(m)
	//var b byte

	for _, f := range mf {
		buf := make([]byte, f.Address.End-f.Address.Start)
		if _, err := m.ReadAt(buf, int64(f.Address.Start)); err != nil {
			panic(err)
		}

		/*
			for b, err = r.ReadByte(); err == nil; b, err = r.ReadByte() {
				if b < 0x80 {
					s.WriteByte(b)
				} else {
					s.WriteByte('.')
				}
			}

			if err != nil {
				panic(err)
			}
		*/

		regions = append(regions, buf)
		//s.Reset()
	}

	pp.Print(regions)
}
