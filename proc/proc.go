package proc

import (
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

// Process contains the process
type Process struct {
	PID int
	pid string
}

// New opens the process
func New(pid int) (*Process, error) {
	p := &Process{
		PID: pid,
		pid: strconv.Itoa(pid),
	}

	if _, err := os.Stat("/proc/" + p.pid + "/"); err != nil {
		return nil, err
	}

	return p, nil
}

// NewSelf returns the current process
func NewSelf() (*Process, error) {
	return New(os.Getpid())
}

// GetMaps returns MapField rows from /proc/$PID/maps
func (p *Process) GetMaps() ([]*MapField, error) {
	f, err := ioutil.ReadFile("/proc/" + p.pid + "/maps")
	if err != nil {
		return nil, err
	}

	regions := strings.Split(string(f), "\n")
	maps := make([]*MapField, 0, len(regions))

	for _, line := range regions {
		f, err := ParseMapField(line)
		if err != nil {
			continue
		}

		maps = append(maps, f)
	}

	return maps, nil
}
