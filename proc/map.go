package proc

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

// MapField is the structure of a line in /proc/$PID/maps
type MapField struct {
	Address AddressRange
	Perms   Perms
	//Offset   uintptr
	Device   Device
	INode    uint
	Pathname string
}

// AddressRange contains the start and end addresses.
type AddressRange struct {
	Start uintptr
	End   uintptr
}

// Device is the structure for the `dev' column. Major:Minor might be int,
// I don't know.
type Device struct {
	Major string
	Minor string
}

// Perms refers to the second column of /proc/$PID/maps. Refer to `man proc`
type Perms uint

const (
	// Read r
	Read Perms = 1 << iota
	// Write w
	Write
	// Execute x
	Execute
	// Shared s
	Shared
	// Private p
	Private
)

// ParseMapField takes something like
//     00400000-00452000 r-xp 00000000 08:02 173521      /usr/bin/dbus-daemon
func ParseMapField(line string) (*MapField, error) {
	fields := strings.Fields(line)
	if len(fields) != 6 {
		return nil, errors.New("Invalid format, needs 6 fields")
	}

	mf := &MapField{}

	// Parse the first column: "00400000-00452000"
	if err := mf.parseAddress(fields[0]); err != nil {
		return nil, err
	}

	// Parse the second column: rwxs
	mf.parsePerms(fields[1])

	// offset is ignored on the third column

	// Parse the fourth column: dev
	if err := mf.parseDevice(fields[3]); err != nil {
		return nil, err
	}

	// Parse the fifth column: inode
	inode, err := strconv.Atoi(fields[4])
	if err != nil {
		return nil, err
	}

	mf.INode = uint(inode)

	// Parse the sixth column: pathname
	mf.Pathname = fields[5]

	return mf, nil
}

// parseAddress parses addresses from format `00400000-00452000`
func (mf *MapField) parseAddress(input string) error {
	r := strings.Split(input, "-")
	if len(r) != 2 {
		return fmt.Errorf("Invalid address format: %s", input)
	}

	s64, err := strconv.ParseUint(r[0], 16, 64)
	if err != nil {
		return fmt.Errorf("Error parsing start range: %#v", err)
	}

	e64, err := strconv.ParseUint(r[1], 16, 64)
	if err != nil {
		return fmt.Errorf("Error parsing end range: %#v", err)
	}

	mf.Address = AddressRange{
		Start: uintptr(s64),
		End:   uintptr(e64),
	}

	return nil
}

// parsePerms parses the perms, rw-p for example
func (mf *MapField) parsePerms(input string) {
	mf.Perms = 0
	for _, r := range input {
		switch r {
		case 'r':
			mf.Perms |= Read
		case 'w':
			mf.Perms |= Write
		case 'x':
			mf.Perms |= Execute
		case 's':
			mf.Perms |= Shared
		case 'p':
			mf.Perms |= Private
		}
	}
}

// parseDevice takes a major:minor (%d:%d) string
func (mf *MapField) parseDevice(input string) error {
	mm := strings.Split(input, ":")
	if len(mm) != 2 {
		return fmt.Errorf("Invalid device: %s", input)
	}

	mf.Device = Device{
		Major: mm[0],
		Minor: mm[1],
	}

	return nil
}
