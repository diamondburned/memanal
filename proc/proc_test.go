package proc

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestSelfProc(t *testing.T) {
	p, err := NewSelf()
	if err != nil {
		t.Fatal(err)
	}

	mfs, err := p.GetMaps()
	if err != nil {
		t.Fatal(err)
	}

	if mfs == nil || len(mfs) == 0 {
		t.Error("No memory map fields found")
	}

	spew.Dump(mfs)
}
