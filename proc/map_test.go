package proc

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestParseMapField(t *testing.T) {
	const m = "00400000-00452000 r-xp 00000000 08:02 173521      /usr/bin/dbus-daemon"
	mf, err := ParseMapField(m)
	if err != nil {
		t.Fatal(t)
	}

	spew.Dump(mf)
}
