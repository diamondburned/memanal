package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
)

type dumbStruct struct {
	inAwe   bool
	content string
	id      int
}

func main() {
	if !(len(os.Args) > 1 && os.Args[1] == "fork") {
		println("Starting", os.Args[0])
		if err := exec.Command(os.Args[0], "fork").Start(); err != nil {
			panic(err)
		}

		return
	}

	ds := dumbStruct{
		inAwe:   true,
		content: "This is a test string",
		id:      34754,
	}

	var test = "this is also a test string, but isn't in a struct"

	fmt.Fprint(ioutil.Discard, ds)
	fmt.Fprint(ioutil.Discard, test)

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Interrupt)
	<-ch
}
